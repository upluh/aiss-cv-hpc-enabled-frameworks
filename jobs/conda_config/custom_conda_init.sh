
# custom function that loads conda (so it does not start conda by default)
function conda_init() {
    __conda_setup="$('/opt/bwhpc/common/devel/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/opt/bwhpc/common/devel/miniconda3/etc/profile.d/conda.sh" ]; then
            . "/opt/bwhpc/common/devel/miniconda3/etc/profile.d/conda.sh"  # commented out by conda i$        else
            export PATH="/opt/bwhpc/common/devel/miniconda3/bin:$PATH"  # commented out by conda init$
        fi
    fi
    unset __conda_setup
}
